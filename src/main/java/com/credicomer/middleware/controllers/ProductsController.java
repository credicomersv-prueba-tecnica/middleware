package com.credicomer.middleware.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.credicomer.middleware.dtos.ResponseDto;
import com.credicomer.middleware.requests.RequestProductsClient;
import com.credicomer.middleware.services.ServiceProductsClient;

@RestController
@RequestMapping("products")
public class ProductsController {

	ServiceProductsClient serviceProductsClien;

	public ProductsController(ServiceProductsClient serviceProductsClien) {
		this.serviceProductsClien = serviceProductsClien;
	}
	@PostMapping
	public ResponseEntity<ResponseDto> getProducts(@RequestBody RequestProductsClient requestData,
			@RequestHeader String credentials) {
		return serviceProductsClien.getProductsClient(requestData, credentials);
	}
}
