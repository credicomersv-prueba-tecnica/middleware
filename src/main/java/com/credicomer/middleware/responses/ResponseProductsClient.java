package com.credicomer.middleware.responses;

import java.util.List;

import com.credicomer.middleware.dtos.Product;

public class ResponseProductsClient {
	List<Product> products;
	String codeError;
    String message;
    String messageError;
	public ResponseProductsClient() {
		super();
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public String getCodeError() {
		return codeError;
	}
	public void setCodeError(String codeError) {
		this.codeError = codeError;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageError() {
		return messageError;
	}
	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}
}
