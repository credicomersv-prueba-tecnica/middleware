package com.credicomer.middleware.requests;

public class RequestProductsClient {
	String clientCode;
	String typeProduct;
	public RequestProductsClient() {
		super();
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getTypeProduct() {
		return typeProduct;
	}
	public void setTypeProduct(String typeProduct) {
		this.typeProduct = typeProduct;
	}
}
