package com.credicomer.middleware.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.credicomer.middleware.dtos.Product;
import com.credicomer.middleware.dtos.ResponseDto;
import com.credicomer.middleware.requests.RequestProductsClient;
import com.credicomer.middleware.services.ServiceProductsClient;

@Service
public class ServiceProductsClientImpl implements ServiceProductsClient {

	private static final String[] firstNames = { "Carlos", "Ana", "Luis", "Maria", "Jose", "Lucia", "Juan", "Sofia",
			"Miguel", "Elena" };

	private static final String[] lastNames = { "Garcia", "Martinez", "Rodriguez", "Lopez", "Hernandez", "Gonzalez",
			"Perez", "Sanchez", "Ramirez", "Torres" };

	@Override
	public ResponseEntity<ResponseDto> getProductsClient(RequestProductsClient requestData, String credentials) {

		if (credentials == null) {
			return new ResponseEntity<>(
					new ResponseDto("Encabezado credentials es requerido", "Encabezado credentials es requerido", null),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Integer clienCode;
		try {
			clienCode = Integer.valueOf(requestData.getClientCode());
		} catch (Exception e) {
			clienCode = 0;
		}
		List<Product> products = new ArrayList<>();
		if (clienCode >= 5 && clienCode <= 10) {
			int randomInt = this.generateRandomInteger();
			for (int i = 1; i <= randomInt; i++) {
				products.add(new Product("", requestData.getClientCode(), String.valueOf(this.generateRandomLong() ), "Cuentas de Ahorro","CC",
						this.generateRandomMoney(), this.generateRandomMoney(), "Cuentas de Ahorro", "01/01/2023",
						"#FHY1234", "-", "1", "78547859", "N"));
			}
		}
		return new ResponseEntity<>(new ResponseDto("Proceso ejecutado con éxito", null, products), HttpStatus.OK);
	}

	public double generateRandomMoney() {
		Random random = new Random();
		int randomAmount = 100 + random.nextInt(801);
		return (double) randomAmount;
	}

	public int generateRandomInteger() {
		Random random = new Random();
		int randomAmount = 1 + random.nextInt(10);
		return randomAmount;
	}

	public Long generateRandomLong() {
		Random random = new Random();
		Long randomAmount = 5289564 + random.nextLong(589458965);
		return randomAmount;
	}

	public static String generateRandomFullName() {
		Random random = new Random();
		String firstName = firstNames[random.nextInt(firstNames.length)];
		String lastName = lastNames[random.nextInt(lastNames.length)];
		return firstName + " " + lastName;
	}

}
