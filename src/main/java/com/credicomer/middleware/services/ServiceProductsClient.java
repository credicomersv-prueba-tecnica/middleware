package com.credicomer.middleware.services;

import org.springframework.http.ResponseEntity;

import com.credicomer.middleware.dtos.ResponseDto;
import com.credicomer.middleware.requests.RequestProductsClient;

public interface ServiceProductsClient {

	ResponseEntity<ResponseDto> getProductsClient(RequestProductsClient requestData, String credentials);

}
