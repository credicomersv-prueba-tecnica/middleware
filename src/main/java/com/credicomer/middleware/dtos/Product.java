package com.credicomer.middleware.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
	@JsonProperty("BUSINESS_CODE")
	private String businessCode;

	@JsonProperty("CLIENT_CODE")
	private String clientCode;

	@JsonProperty("PRODUCT_NUMBER")
	private String productNumber;

	@JsonProperty("PRODUCT_TYPE")
	private String productType;

	@JsonProperty("SYSTEM_CODE")
	private String systemCode;

	@JsonProperty("AVAILABLE_BALANCE")
	private Double availableBalance;

	@JsonProperty("ACCOUNT_BALANCE")
	private Double accountBalance;

	@JsonProperty("SYSTEM_DESCRIPTION")
	private String systemDescription;

	@JsonProperty("NEXT_PAYMENT_DATE")
	private String nextPaymentDate;

	@JsonProperty("COLOR_CODE")
	private String colorCode;

	@JsonProperty("PRODUCT_ALIAS")
	private String productAlias;

	@JsonProperty("STATUS_CODE")
	private String statusCode;

	@JsonProperty("TRANSFER365_MOVIL")
	private String transfer365Movil;

	@JsonProperty("ROTARY_LINE")
	private String rotaryLine;

	public Product() {
		super();
	}

	public String getBusinessCode() {
		return businessCode;
	}

	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public Double getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public Double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getSystemDescription() {
		return systemDescription;
	}

	public void setSystemDescription(String systemDescription) {
		this.systemDescription = systemDescription;
	}

	public String getNextPaymentDate() {
		return nextPaymentDate;
	}

	public void setNextPaymentDate(String nextPaymentDate) {
		this.nextPaymentDate = nextPaymentDate;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTransfer365Movil() {
		return transfer365Movil;
	}

	public void setTransfer365Movil(String transfer365Movil) {
		this.transfer365Movil = transfer365Movil;
	}

	public String getRotaryLine() {
		return rotaryLine;
	}

	public void setRotaryLine(String rotaryLine) {
		this.rotaryLine = rotaryLine;
	}

	public Product(String businessCode, String clientCode, String productNumber, String productType, String systemCode,
			Double availableBalance, Double accountBalance, String systemDescription, String nextPaymentDate,
			String colorCode, String productAlias, String statusCode, String transfer365Movil, String rotaryLine) {
		super();
		this.businessCode = businessCode;
		this.clientCode = clientCode;
		this.productNumber = productNumber;
		this.productType = productType;
		this.systemCode = systemCode;
		this.availableBalance = availableBalance;
		this.accountBalance = accountBalance;
		this.systemDescription = systemDescription;
		this.nextPaymentDate = nextPaymentDate;
		this.colorCode = colorCode;
		this.productAlias = productAlias;
		this.statusCode = statusCode;
		this.transfer365Movil = transfer365Movil;
		this.rotaryLine = rotaryLine;
	}
}
