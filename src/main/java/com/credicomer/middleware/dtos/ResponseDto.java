package com.credicomer.middleware.dtos;

import java.io.Serializable;

public class ResponseDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 993367462771180114L;

	/** The message. */
	private String message;

	/** The errors. */
	private Object errors;

	/** The response. */
	private Object response;

	/**
	 * Instantiates a new abstract response DTO.
	 */
	public ResponseDto() {
		super();
	}

	/**
	 * Instantiates a new abstract response DTO.
	 *
	 * @param code    the code
	 * @param message the message
	 */
	public ResponseDto(String message) {
		super();
		this.message = message;
	}

	/**
	 * Instantiates a new abstract response DTO.
	 *
	 * @param code     the code
	 * @param message  the message
	 * @param errors   the errors
	 * @param response the response
	 */
	public ResponseDto(String message, Object errors, Object response) {
		super();
		this.message = message;
		this.errors = errors;
		this.response = response;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the response.
	 *
	 * @return the response
	 */
	public Object getResponse() {
		return response;
	}

	/**
	 * Sets the response.
	 *
	 * @param response the new response
	 */
	public void setResponse(Object response) {
		this.response = response;
	}

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public Object getErrors() {
		return errors;
	}

	/**
	 * Sets the errors.
	 *
	 * @param errors the new errors
	 */
	public void setErrors(Object errors) {
		this.errors = errors;
	}

}
